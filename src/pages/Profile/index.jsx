import { Box, Container, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import { FormEditProfile } from "../../Components/FormEditProfile";

const useStyles = makeStyles((theme) => ({
  root: {
    display:"flex",
    flexDirection:"column",
    justifyContent:"center",
    alignItems:"center",
    margin: '3rem auto',
    padding:theme.spacing(2),
    borderRadius: "10px",
    backgroundColor: theme.palette.background.paper
  }
}))
export const Profile = () => {
  const classes = useStyles();
  return (
    <Container maxWidth="sm">
      <Box className={classes.root}>
        <Typography variant="h5" color="textPrimary">
          Información Relacionada con la cuenta
        </Typography>
        <FormEditProfile/>
      </Box>
    </Container>
  );
};
