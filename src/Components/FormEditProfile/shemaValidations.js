import * as yup from 'yup';
const message = "Campo requerido"
export const shemaValidations = yup.object().shape({
    name: yup.string().required(message),
    lastName: yup.string().required(message),
    age: yup.string().required(message),
    location: yup.string().required(message),
    phone: yup.string().required(message),
})