import "@testing-library/jest-dom";
import configureStore from "redux-mock-store";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { Welcome } from "../../../Components/Welcome";
import { appReducers } from "../../../store/reducers";
import { types } from "../../../store/user/types";

const middlewares = [];
const mockStore = configureStore(middlewares);
describe(`Pruebas en <Welcome/>`, () => {
  const user = {
    name: "Sebastian",
    lastName: "Botero",
  };

  const initialState = {
    user: {
      user: user,
    },
    auth: {
      auth: {
        idUser: "2346273456",
      },
    },
  };
  const store = mockStore(initialState);
  const wrapper = mount(
    <Provider store={store}>
      <Welcome />
    </Provider>
  );

  const getUser = () => ({
    type: types.getUser,
    payload: user,
  });

  test("Debe mostrarse correctamente", () => {
    store.dispatch(getUser());
    expect(wrapper).toMatchSnapshot();
  });

  test("Debe mostrar el nombre del usuario correctamente", () => {
    const value = `Bienvenido  ${initialState.user.user.name} ${initialState.user.user.lastName} a sofware S.A.S`;
    expect(wrapper.find("h4").text()).toBe(value);
  });
});
