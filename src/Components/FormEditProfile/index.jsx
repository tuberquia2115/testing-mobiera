import React from "react";
import { Box, Button, Grid, makeStyles } from "@material-ui/core";
import { SaveTwoTone } from "@material-ui/icons";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { yupResolver } from "@hookform/resolvers/yup";

import { ControllerInput } from "../../Components/UI/ControllerInput";
import { doEditUser } from "../../store/user/actions";
import { shemaValidations } from "./shemaValidations";

const useStyles = makeStyles((theme) => ({
  containerForm: {
    padding: "2rem",
    width: "100%",
    height: "100%",
  },
  buttonSession: {
    marginTop: theme.spacing(3),
  },
  form: {
    flexGrow: 1,
    width: "100%",
  },
}));
export const FormEditProfile = () => {
  const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const classes = useStyles();
  const { handleSubmit, control } = useForm({
    defaultValues: user !== null ? user : {},
    resolver: yupResolver(shemaValidations),
    mode: "all",
  });

  const onSubmit = (values) => {
    dispatch(doEditUser(values));
  };

  return (
    <Box className={classes.containerForm}>
      <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
        <Grid container xs={12} spacing={1}>
          <Grid item xs={12} spacing={3}>
            <ControllerInput
              name="name"
              control={control}
              required={true}
              type="text"
              label="Nombre"
              fullWidth={true}
              margin="dense"
              variant="outlined"
            />
            <ControllerInput
              name="lastName"
              control={control}
              required={true}
              type="text"
              label="Apellidos"
              fullWidth={true}
              margin="dense"
              variant="outlined"
            />
            <ControllerInput
              name="age"
              control={control}
              required={true}
              type="text"
              label="Edad"
              fullWidth={true}
              margin="dense"
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} spacing={3}>
            <ControllerInput
              name="phone"
              control={control}
              required={true}
              type="text"
              label="Teléfono"
              fullWidth={true}
              margin="dense"
              variant="outlined"
            />
            <ControllerInput
              name="location"
              control={control}
              required={true}
              type="text"
              label="Dirección de residencia"
              fullWidth={true}
              margin="dense"
              variant="outlined"
            />
            <ControllerInput
              disabled={true}
              name="email"
              control={control}
              required={false}
              type="text"
              label="Correo Eléctronico"
              fullWidth={true}
              margin="dense"
              variant="outlined"
            />
          </Grid>
        </Grid>
        <Button
          size="large"
          type="sudmit"
          variant="contained"
          color="primary"
          disableElevation
          className={classes.buttonSession}
          endIcon={<SaveTwoTone htmlColor="#fff" />}
        >
          Guardar Cambios
        </Button>
      </form>
    </Box>
  );
};
