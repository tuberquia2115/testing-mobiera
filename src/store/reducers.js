import { combineReducers } from "redux";
import { authReducer } from "./auth";
import { userReducer } from "./user";

export const appReducers = combineReducers({
    auth: authReducer,
    user: userReducer
})