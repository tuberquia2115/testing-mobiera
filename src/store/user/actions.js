import { clientAxios } from "../../config/axios";
import { types } from "./types";

const setIsLaoding = (state) => ({
  type: types.setIsLoading,
  payload: state,
});
export const doGetUser = () => {
  return async (dispatch) => {
    dispatch(setIsLaoding(true));
    try {
      const { data } = await clientAxios.get("/user");
      dispatch({
        type: types.getUser,
        payload: data,
      });
      dispatch(setIsLaoding(false));
    } catch (error) {
      console.log(error);
    }
  };
};

export const doEditUser = (newData) => {
  return async (dispatch) => {
    dispatch(setIsLaoding(true));
    try {
      await clientAxios.put("/user", newData);
      dispatch({
        type: types.editUser,
      });
      alert("Los cambios se guardaron correctamente")
      dispatch(doGetUser());
      dispatch(setIsLaoding(false));
    } catch (error) {
      console.log(error);
    }
  };
};
