import { createMuiTheme, responsiveFontSizes } from "@material-ui/core";

const defaultTheme = createMuiTheme();

const theme = createMuiTheme({
  ...defaultTheme,
  palette: {
    primary: {
      main: "#0d283d",
    },
    secondary: {
      main: "#0c447c",
    },
    common: {
      main: "#01579b",
    },
  },
});

const responsiveTheme = responsiveFontSizes(theme);

export { theme as default, responsiveTheme };
