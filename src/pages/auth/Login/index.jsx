import React, { useCallback, useState } from "react";
import {
  Box,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { yupResolver } from "@hookform/resolvers/yup";

import { AuthLayout } from "../../../layouts/auth";
import { shemaValidations } from "./shemaValidatios";
import { ControllerInput } from "../../../Components/UI/ControllerInput";
import { ControllerInputPassword } from "../../../Components/UI/ControllerInputPassword";
import { doSignInWithEmailAndPassword } from "../../../store/auth/actions";
import {flexCenterRow} from '../../../styles'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#FFFFFF",
    borderRadius: "10px",
    width: "40%",
    height: "50%",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  containerForm: {
    padding: "2rem",
    display: "flex",
    height: "100%",
    flexDirection: "column",
    ...flexCenterRow
  },
  buttonSession: {
    marginTop: theme.spacing(3),
  },
}));
export const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const dispatch = useDispatch();

  const classes = useStyles();
  const initialValues = {
    email: "",
    password: "",
  };
  const { handleSubmit, control } = useForm({
    defaultValues: initialValues,
    resolver: yupResolver(shemaValidations),
    mode: "all",
  });

  const onSubmit = (values) => {
    const { email, password } = values;
    dispatch(doSignInWithEmailAndPassword(email, password))
  };

  const handleClickShowPassword = useCallback((state) => {
    setShowPassword(state);
  }, []);
  return (
    <AuthLayout>
      <Box className={classes.root}>
        <Box className={classes.containerForm}>
          <Typography variant="h5" color="textPrimary">
            Login👋🏻
          </Typography>
          <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
            <ControllerInput
              name="email"
              control={control}
              required={true}
              type="email"
              label="Correo"
              fullWidth={true}
              placeholder="ejemplo@gmail.com"
              margin="dense"
              variant="outlined"
            />
            <ControllerInputPassword
              name="password"
              control={control}
              required={true}
              label="Contraseña"
              type="password"
              fullWidth
              margin="dense"
              variant="outlined"
              showPassword={showPassword}
              handleClickShowPassword={() => {
                handleClickShowPassword(!showPassword);
              }}
            />
            <Button
              type="sudmit"
              disableElevation
              variant="contained"
              color="secondary"
              className={classes.buttonSession}
            >
              Iniciar Sesión
            </Button>
          </form>
        </Box>
      </Box>
    </AuthLayout>
  );
};
