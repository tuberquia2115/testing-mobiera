import { initState } from "./init-state";
import { types } from "./types";

export const authReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case types.login:
      return {
        ...state,
        auth: payload,
      };
    case types.logout:
      localStorage.removeItem('user');
      return {
        ...state,
        auth: null,
      };
    default:
      return state;
  }
};
