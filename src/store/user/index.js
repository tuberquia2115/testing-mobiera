import { initState } from "./init-state";
import { types } from "./types";

export const userReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case types.getUser:
      return {
        ...state,
        user: payload,
      };
    case types.setIsLoading:
      return {
        ...state,
        loading: payload,
      };

    default:
      return state;
  }
};
