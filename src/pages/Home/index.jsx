import React from "react";
import { Container } from "@material-ui/core";

import { Welcome } from "../../Components/Welcome";
export const Home = () => {
  return (
    <Container maxWidth="sm">
      <Welcome/>
    </Container>
  );
};
