import React from 'react';
import PropTypes from 'prop-types';
import { Controller } from "react-hook-form";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { IconButton, InputAdornment, TextField } from "@material-ui/core";


export const ControllerInputPassword = ({
  control,
  name,
  required,
  label,
  size,
  color,
  variant,
  fullWidth,
  handleClickShowPassword,
  showPassword,
}) => {
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  return (
    <Controller
      name={name}
      control={control}
      rules={{ required: required }}
      render={({ field, fieldState: { error } }) => (
        <TextField
          type={showPassword ? "text" : "password"}
          label={label}
          margin="dense"
          size={size}
          color={color}
          variant={variant}
          fullWidth={fullWidth}
          error={Boolean(error)}
          helperText={error && error.message}
          {...field}
          InputProps={{
            endAdornment: (
              <InputAdornment position="starts">
                <IconButton
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      )}
    />
  );
};

ControllerInputPassword.propTypes = {
  control: PropTypes.node.isRequired,
  name: PropTypes.string.isRequired,
  required: PropTypes.bool,
  size: PropTypes.string,
  color: PropTypes.string,
  label: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
  fullWidth: PropTypes.bool,
  handleClickShowPassword: PropTypes.func.isRequired,
  showPassword: PropTypes.bool
};