import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { Home } from "../pages/Home";
import { Profile } from "../pages/Profile";
import { Header } from "../Components/Header";

export const HomeRoute = () => {
  return (
    <div>
      <Header />
      <Switch>
        <Route exact strict path="/home" component={Home} />
        <Route exact strict path="/home/profile" component={Profile} />
        <Redirect to="/home" />
      </Switch>
    </div>
  );
};
