import React from "react";
import PropTypes from "prop-types";
import { Box, makeStyles } from "@material-ui/core";

import { flexCenterRow } from "../../styles";
import IMAGE from "../../assets/imgs/fondo.jpeg";

const useStyles = makeStyles(() => ({
  root: {
    ...flexCenterRow,
    width: "100%",
    height: "100vh",
    backgroundPosition: "center",
    backgroundSize: "100% 100%",
    backgroundRepeat: "no-repea",
  },
}));

export const AuthLayout = ({ children }) => {
  const classes = useStyles();
  return (
    <Box>
      <Box
        className={classes.root}
        style={{ backgroundImage: `url(${IMAGE})` }}
      >
        {children}
      </Box>
    </Box>
  );
};

AuthLayout.propTypes = {
  children: PropTypes.node,
};
