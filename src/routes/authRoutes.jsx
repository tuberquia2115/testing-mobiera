import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { Login } from "../pages/auth/Login";

export const AuthRoute = () => {
  return (
    <Switch>
      <Route exact strict path="/auth/login" component={Login} />
      <Route exact strict path="/auth/register" component={<div>Componente register</div>} />
      <Redirect to="/auth/login" />
    </Switch>
  );
};
