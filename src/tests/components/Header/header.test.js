import React from "react";
import "@testing-library/jest-dom";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import { mount } from "enzyme";
import { Router } from "react-router-dom";
import { Header } from "../../../Components/Header";
import { types } from "../../../store/user/types";

const middlewares = [];
const mockStore = configureStore(middlewares);
describe(`Pruebas en <Header/>`, () => {
  const user = {
    name: "Sebastian",
    lastName: "Botero",
  };
  const initialState = {
    user: {
      user: user,
    },
    auth: {
      auth: {
        idUser: "2346273456",
      },
    },
  };

  const store = mockStore(initialState);
  const historyMock = {
    location: jest.fn(),
    listen: jest.fn(),
    createHref: jest.fn(),
  };
  const wrapper = mount(
    <Provider store={store}>
      <Router history={historyMock}>
        <Header />
      </Router>
    </Provider>
  );

  afterEach(() => {
    jest.clearAllMocks();
  });

  const getUser = () => ({
    type: types.getUser,
    payload: user,
  });
  test(`debe mostrarse correctamente`, () => {
    store.dispatch(getUser());
    expect(wrapper).toMatchSnapshot();
  });

  test('Debe mostrar el nombre del usuario correctamente', () => {
    const value = `${initialState.user.user.name} ${initialState.user.user.lastName}`
    expect(wrapper.find('a').at(0).text()).toBe(value)
  })
  
});
