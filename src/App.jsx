import React from "react";
import { Provider } from "react-redux";
import { LayoutMain } from "./layouts/main";
import { AppRoutes } from "./routes";

import { store } from "./store";

function App() {
  return (
    <Provider store={store}>
      <LayoutMain>
        <AppRoutes />
      </LayoutMain>
    </Provider>
  );
}

export default App;
