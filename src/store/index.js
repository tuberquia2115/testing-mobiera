import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { appReducers } from "./reducers";

export const store = createStore(
  appReducers,
  compose(
    applyMiddleware(thunk),
    typeof window === "object" &&
      typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined"
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : (f) => f
  )
);
