import React from "react";
import PropTypes from "prop-types";
import { TextField } from "@material-ui/core";
import { Controller } from "react-hook-form";

export const ControllerInput = ({
  control,
  name,
  required,
  disabled,
  type,
  size,
  color,
  placeholder,
  label,
  variant,
  fullWidth,
  helperText,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{ required: required }}
      render={({ field, fieldState: { error } }) => (
        <TextField
          disabled={disabled}
          type={type}
          label={label}
          placeholder={placeholder}
          margin="dense"
          variant={variant}
          size={size}
          color={color}
          fullWidth={fullWidth}
          error={Boolean(error)}
          helperText={error ? error.message : helperText}
          {...field}
        />
      )}
    />
  );
};

ControllerInput.propTypes = {
  control: PropTypes.node.isRequired,
  name: PropTypes.string.isRequired,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
  fullWidth: PropTypes.bool,
  helperText: PropTypes.string,
};
