import React from "react";
import PropTypes from "prop-types";
import { MuiThemeProvider, CssBaseline } from "@material-ui/core";
import theme from '../styles/theme'

export const LayoutMain = ({ children }) => {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <main>{children}</main>
    </MuiThemeProvider>
  );
};
LayoutMain.propTypes = {
  children: PropTypes.node,
};
