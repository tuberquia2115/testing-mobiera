# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## `json-server --watch db.json --port 3004`
Ejecuta este comando para subir el servidor, que simula la base de datos.


## Autenticación

Puedes iniciar sesión con las siguientes credenciales 
Correo: testing@gmail.com
password: 123456
.

