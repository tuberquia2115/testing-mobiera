import React from "react";
import { makeStyles } from "@material-ui/core";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles(() => ({
  navContainer: {
    width: "auto",
    padding: "1em auto",
    fontSize: "20px",
    "& .active": {
      borderBottom: "5px",
      borderBottomColor: "#FFF",
      borderBottomStyle: "solid",
    },
  },
  list: {
    listStyle: "none",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    fontFamily: "Roboto, sans-serif",
  },

  link: {
    textDecoration: "none",
    color: "#FFFFFF",
    paddingBottom: "0.2em",
    "&:hover": {
      borderBottom: "5px",
      borderBottomColor: "#FFF",
      borderBottomStyle: "solid",
    },
  },
  listItem: {
    margin: "auto 1rem",
  },
}));
export const NavBar = () => {
  const classes = useStyles();
  return (
    <nav className={classes.navContainer}>
      <ul className={classes.list}>
        <li className={classes.listItem}>
          <NavLink
            activeClassName="active"
            to="/home/profile"
            className={classes.link}
          >
            Perfil
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};
