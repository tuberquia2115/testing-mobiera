import React from "react";
import { useSelector } from "react-redux";
import { Box, Typography } from "@material-ui/core";

export const Welcome = () => {
  const {user} = useSelector(state => state.user)
  return (
    <Box
      margin="11rem auto"
      padding={3}
      borderRadius={10}
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
      display="flex"
      bgcolor="#F3F3F3"
    >
      <Typography variant="h4" color="textPrimary" align="center">
        Bienvenido  {`${user?.name} ${user?.lastName}`} a sofware S.A.S
      </Typography>
      <Typography variant="body1" color="textSecondary" align="justify">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima quo
        pariatur repudiandae saepe blanditiis! Ipsa magni nam aliquid animi, ad
        recusandae reiciendis aut sequi ullam sit quos cum officiis optio.
      </Typography>
    </Box>
  );
};
