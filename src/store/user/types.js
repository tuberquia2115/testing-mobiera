export const  types= {
    getUser: "[user] getUser",
    editUser: "[user] editUser",
    setIsLoading: "[loading] setIsLoading"
}