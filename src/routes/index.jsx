import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import { doGetUser } from "../store/user/actions";
import { AuthRoute } from "./authRoutes";
import { HomeRoute } from "./homeRoutes";
import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoutes";

export const AppRoutes = () => {
  const dispatch = useDispatch();
  const { auth } = useSelector((state) => state.auth);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    if (auth?.idUser) {
      dispatch(doGetUser());
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, [auth, dispatch, setIsLoggedIn]);
  return (
    <BrowserRouter>
      <PublicRoute path="/auth" isLoggedIn={isLoggedIn} component={AuthRoute} />
      <PrivateRoute
        path="/"
        isLoggedIn={isLoggedIn}
        component={HomeRoute}
      />
    </BrowserRouter>
  );
};
