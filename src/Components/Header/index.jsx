import React from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { ExitToAppTwoTone } from "@material-ui/icons";
import { AppBar, Toolbar, makeStyles, IconButton } from "@material-ui/core";

import { NavBar } from "../UI/NavBar";
import { doSignInt } from "../../store/auth/actions";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    textDecoration: "none",
    color: "#FFF",
    fontSize: "20px",
  },
  toolbar: {
    justifyContent: "space-between",
    display: "flex",
  },
}));
export const Header = () => {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.user);
  const classes = useStyles();

  const handleLogout = () => {
    dispatch(doSignInt());
  };
  return (
    <div className={classes.root}>
      <AppBar position="static" color="primary" elevation={0}>
        <Toolbar className={classes.toolbar} >
          <Link to="/" className={classes.title}>
            {`${user?.name} ${user?.lastName}`}
          </Link>
          <NavBar />
          <IconButton  onClick={() => handleLogout()}>
            <ExitToAppTwoTone htmlColor="#FFF" fontSize="large" />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
};
