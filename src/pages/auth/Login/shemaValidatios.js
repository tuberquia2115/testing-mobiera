import * as yup from 'yup';
const message = "Campo requerido"
export const shemaValidations = yup.object().shape({
    email: yup.string().email("El email no es valido").required(message),
    password: yup.string().required(message)
})